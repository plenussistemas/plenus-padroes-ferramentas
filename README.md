# Plenus Sistemas | Padrões & Ferramentas

Este repositório foi criado com o objetivo geral de padronizar e homogeneizar ao máximo possível
a equipe de desenvolvimento, bem como os projetos desenvolvidos.

Para tal, foram criados os seguintes objetivos específicos:

1. Estabelecer os padrões gerais que a equipe de desenvolvimento deve seguir no dia a dia;
2. Padronizar o uso do [**Git**](./padroes/git/README.md);
3. Elucidar e padronizar todas as ferramentas usadas pela equipe de desenvolvimento;
4. Criar manuais, se assim necessários, dessas ferramentas;

Para que consigamos atingir esses objetivos, é muito importante que todos da equipe mantenham este repositório sempre atualizado.

Vamos lá, contribua você também! =)

---

## Como funciona?

O repositório está divido em três pastas:

* Ferramentas: Contém informações sobre como instalar os softwares necessários ao nosso cotidiano.
* Padrões: Contém informações sobre os padrões que devem ser seguidos pelos membros da equipe.
* Projetos: Contém informações específicas sobre os principais projetos desenvolvidos pela Plenus.

---

## Não sabe por onde começar?

Comece pela pasta [**Git**](./padroes/git/README.md), ela contém as explicações necessárias para você contribuir com este e todos os outros repositórios e projetos da Plenus.
