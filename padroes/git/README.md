## Plenus Sistemas | Git

Para obtermos maior tranquilidade e segurança do código que vai para produção, todos os nossos repositórios são privados, ou seja,
somente um usuário tem a permissão de realizar push de novas linhas de códigos para produçao.

Dessa forma, como outros usuários irão contribuir para os repositórios da Plenus?

Eles irão contribuir através de Pull Requests. Em resumo, os outros usuários, que não são donos do repositório, terão que 
fazer um fork do repositório original, realizar as alterações necessárias, e solicitar o Pull Request. Essa solicitação passará
por um Code Review, e se aprovada, somente nesse momento irá para produção.

Essas etapas de Repositório Privado, Fork, Pull Request e Code Review, proporcionam segurança tanto para a Empresa, quanto
para quem está desenvolvendo novas funcionalidades, pois garantimos que tudo que está indo para produção está sendo revisado e testado.

Está muito confuso? Fique calmo, iremos explicar detalhadamente o passo a passo a ser seguido.

---

##### Não está familiarizado com o Git?
Veja esse [**site**](https://rogerdudler.github.io/git-guide/index.pt_BR.html), nele você encontra os comandos Git mais usados.

---

##### Por onde começar?

Comece pelo Bitbucket.

1. Crie sua conta ou se autentique no [Bitbucket](https://bitbucket.org/account/signup/);
2. Solicite a URL do repositório que você irá trabalhar;
3. Solicite a autorização para visualizar o repositório, pois sem ela, você terá a URL, mas não conseguirá, 
ver o repositório, fazer fork ou realizar pull requests;
4. Após autorizada sua solicitação, você receberá um convite em seu e-mail para visualizar o repositório.
Encontre o convite em seu e-mail e aceite-o;

---

##### Dê um fork no projeto

Lembre-se de instalar o [**Fork**](../../ferramentas/fork/README.md), ele será necessário logo mais.  
Acreditando que você tenha realizado as etapas anteriores, siga os seguintes passos:

1. Acesse a URL raiz do repositório que você irá trabalhar, por exempo, a URL raiz desse repositório é   
`https://bitbucket.org/plenussistemas/plenus-padroes-ferramentas/src/master`;
2. No menu lateral esquerdo, clique no terceiro ícone de cima para baixo "Create" e depois em "**Fork** this repository";
3. Nesse momento você está fazendo um fork do repositório original, é como se você estivesse fazendo uma cópia do repositório original,
para o **seu** repositório remoto. Em vista disso, em "Name", você pode preencher o nome que preferir ou deixar o sugerido, pois esse será
o nome da sua cópia. Finalizando, clique em "Fork repository". Se tudo deu certo, você já será redirecionado para a sua cópia do repositório;
4. Perceba que você não está mais no repositório original, a URL do repositório original é algo parecido com:  
`https//bitbucket.org/plenussistemas/[NOME_DO_REPOSITORIO]/src/master`  
E o seu fork deve ter a URL parecida com:   
`https//bitbucket.org/[SEU_NOME_DE_USUARIO]/[NOME_QUE_VOCE_ESCOLHEU_NO_FORK]/src/master`

Se você chegou até aqui, e deu tudo certo, **parabéns!** Você já tem um fork do repositório o qual você precisa trabalhar.

---

##### Bem iniciado, metade feito
Agora que você conseguiu realizar as etapas iniciais, confira a [**documentação**](./workflow.md) do nosso workflow
com o Git.
