# Plenus Localidades

## O que é?

(tbd)



## Como instalar?

1. Certifique-se de ter em sua máquina as ferramentas: [PHP](../../ferramentas/php/README.md), [Composer](../../ferramentas/composer/README.md) e [PostgreSQL](../../ferramentas/postgresql/README.md). 

2. Em algum terminal, teste os seguintes comandos: 
   * `php -v` : Deve exibir versão 7.3.
   * `composer -V` : Deve exibir uma versão recente, atualize se necessário. 
   * `psql -V` : Deve exibir versão 11 ou superior. 

3. Em um terminal, execute `psql -h localhost -p 5432 -U postgres` e informe a senha desse usuário (geralmente "postgres").

4. Execute o SQL `CREATE DATABASE localidades;` para criar um banco de dados vazio. Utilize alguma ferramenta de banco de dados para verificar se a criação ocorreu.

5. No repositório da empresa, procure por "plenus-localidades" e clone-o em sua máquina. É necessário receber permissão especial para acessar esse projeto.

6. Em um terminal, navegue até a pasta onde o projeto foi clonado (a qual chamaremos de RAIZ) e execute `composer install` para baixar as dependências do projeto.

7. Ainda na raiz do projeto, duplique o arquivo ".env.example", renomeie-o para ".env" e edite seu conteúdo para incluir os dados de acesso que serão fornecidos pela equipe.

8. No terminal, após instalação completa das dependências, execute `php artisan migrate --seed` para criar a estrutura inicial no banco de dados.

9.  Existem 2 modos de servir a aplicação.
    * Configurar um virtual host no Apache, apontando-o para "{raiz_projeto}/public/";
    * Executar `php -S localhost:3000 -t public` em um terminal e deixá-lo aberto.

10. Nesse momento, a aplicação "Plenus Localidades" já está pronta para uso.



## Observações

* O Localidades foi desenvolvido com base no micro-framework Lumen, portanto, não possui as mesmas funcionalidades e comandos de uma aplicação Laravel (Ex: "artisan serve", "artisan make").

* Esse projeto não depende de outros para sua execução, mas serve como dependência para o Legislarr e Carta de Serviços.

* Lembrar que ao adicionar ou atualizar uma dependência no projeto, deve também atualizar em produção (`git pull` + `composer update`).
