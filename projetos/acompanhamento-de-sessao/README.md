# Acompanhamento de Sessão

## O que é?

(tbd)



## Como instalar?

1. Certifique-se de ter concluido a instalação e configuração do projeto [Legislarr](../legislarr-admin/README.md).

2. Certifique-se de ter em sua máquina as ferramentas: [NodeJS](../../ferramentas/nodejs/README.md) e [Redis](../../ferramentas/redis/README.md). 

3. Em algum terminal, teste os seguintes comandos: 
   * `node -v` : Deve ser a LTS mais recente. 
   * `npm -v` : Deve exibir algo. 

4. Abra o "Gerenciador de Tarefas" do Windows, vá para a aba "Serviços" e procure pelo nome "Redis". O serviço deve existir e estar com status "rodando". 

5. No repositório da empresa, procure por "sessao" e clone-o em sua máquina. É necessário receber permissão especial para acessar esse projeto.

6. No terminal e execute `npm install -g laravel-echo-server` e `npm install -g @vue/cli` para que essas dependências fiquem disponíveis a outros projetos. 

7. Ainda no terminal, navegue até a pasta onde o projeto foi clonado (a qual chamaremos de RAIZ) e execute `npm install` para baixar as dependências.

8. Em "public/index.html" confirmar se a seguinte linha está comentada:
`<script src="//admin.legislarr.com.br:6001/socket.io/socket.io.js"></script>`

9. Em "public/index.html" confirmar se a seguinte linha está descomentada:
`<script src="//localhost:6001/socket.io/socket.io.js"></script>`

10. Navegue até a raiz do projeto "Legislarr Admin" e abra três terminais. 

11. Em um deles, sirva a aplicação com `php artisan serve`. No outro, abra o canal de comunicação com `laravel-echo-server start`. Por último, `php artisan queue:listen --tries=1`. Mantenha os terminais abertos.

12. Na raiz do projeto "Acompanhamento de Sessão", execute `npm run serve`.



## Observações

* Em produção, os passos 8 e 9 são invertidos, ou seja, a linha descomentada deve ser a que contém o endereço real do Legislarr.