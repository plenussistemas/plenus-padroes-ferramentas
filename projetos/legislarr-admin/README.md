# Legislarr Admin

## O que é?

(tbd)



## Como instalar?

1. Certifique-se de ter em sua máquina as ferramentas: [PHP](../../ferramentas/php/README.md), [Composer](../../ferramentas/composer/README.md), [NodeJS](../../ferramentas/nodejs/README.md), [PostgreSQL](../../ferramentas/postgresql/README.md) e [Redis](../../ferramentas/redis/README.md). 

2. Em algum terminal, teste os seguintes comandos: 
   * `php -v` : Deve exibir versão 7.3.
   * `composer -V` : Deve exibir uma versão recente, atualize se necessário. 
   * `psql -V` : Deve exibir versão 11 ou superior. 
   * `node -v` : Deve ser a LTS mais recente. 
   * `npm -v` : Deve exibir algo. 

3. Em um terminal, execute `psql -h localhost -p 5432 -U postgres` e informe a senha desse usuário (geralmente "postgres").

4. Execute o SQL `CREATE DATABASE legislarr;` para criar um banco de dados vazio. Utilize alguma ferramenta de banco de dados para verificar se a criação ocorreu.

5. No repositório da empresa, procure por "legislarr-admin" e clone-o em sua máquina. É necessário receber permissão especial para acessar esse projeto.

6. Em um terminal, navegue até a pasta onde o projeto foi clonado (a qual chamaremos de RAIZ) e execute `composer install` para baixar as dependências do projeto.

7. Ainda na raiz do projeto, duplique o arquivo ".env.example", renomeie-o para ".env" e edite seu conteúdo para incluir os dados de acesso que serão fornecidos pela equipe.

8. Usando o Filezilla, teste se é possível acessar o FTP com usuário e senha do arquivo ".env".

9. Após instalação completa das dependências, execute `php artisan storage:link` para criar um link que permitirá acesso aos arquivos de upload. Lembrete: Esse link fica na pasta "public" e deve ser excluido e recriado caso a pasta raiz seja movida ou renomeada.

10. Ainda no terminal, execute `php artisan migrate --seed` para criar a estrutura inicial no banco de dados.

11. Em seguida, execute `php artisan migrate --dbInitial --seedInitial --all`. As opções "dbInitial", "seedInitial", "schema=XXX" e "all" são comandos customizados. Em caso de dúvida, o comando `php artisan migrate --help` exibe uma breve descrição dessas opções.

12. Nesse momento, o banco de dados "legislarr" estará pronto para uso.

13. Existem 2 modos de servir a aplicação.
    * Configurar um virtual host no Apache, apontando-o para "{raiz_projeto}/public/";
    * Executar `php artisan serve` em um terminal e deixá-lo aberto. Nesse caso, a URL é "localhost:8000".

14. Em um terminal, navegue até a raiz e execute `laravel-echo-server init`. Responder as perguntas da seguinte forma:
    * Do you want to run this server in development mode? (y) [enter]
    * Which port would you like to serve from? (6001) [enter]
    * Which database would you like to use to store presence channel members? >redis [enter]
    * Enter the host of your Laravel authentication server (http://localhost:8000) [enter]
    * Will you be serving on http or https? >http [enter]
    * Do you want to generate a client ID/Key for HTTP API? (y) [enter]
    * Do you want to setup cross domain access to the API? (y) [enter]
    * Specify the URI that may access the API: (http://localhost:8000) [enter]
    * Enter the HTTP methods that are allowed for CORS: (GET, POST) [enter]
    * Enter the HTTP headers that are allowed for CORS: (Origin, Content-Type, X-Auth-Token, X-Requested
      -With, Accept, Authorization, X-CSRF-TOKEN, X-Socket-Id) [enter]
    * What do you want this config to be saved as? (laravel-echo-server.json) [enter]

15. Nesse momento, a aplicação "Legislarr" já está pronta para uso, seja pelo navegador ou API.



## Cadastrando um cliente

1. Deve existir algum cliente previamente cadastrado. Para testes, o padrão é "camaramaringa.com.br".

2. Sirva a aplicação e, em um navegador, acesse "{url_do_projeto}/login/camaramaringa.com.br".

3. Surgirá uma tela de login, com o nome do órgão fictício "Câmara Maringá". A equipe lhe fornecerá os dados de acesso.

4. Navegue até "/cliente".

5. Clique no botão "Novo" e preencha todos os campos. A versão de API é 1. Domínio, schema e imagem de brasão devem ser reais. 
O padrão de nome do schema é "pm_{cidade}" ou "cm_{cidade}". 

6. Clique em "Salvar" e aguarde a mensagem de sucesso.

7. Usando a ferramenta de banco de dados, abra o banco "legislarr" e crie manualmente um schema com o mesmo nome usado no passo 5.

8. Em um terminal, execute `php artisan migrate --schema={nome_schema} --seedInitial`.

9. No banco de dados, acesse o esquema recém criado e abra a tabela "modulo". Remova as linhas que correspondam a módulos que
não serão utilizados pelo cliente, exceto as linhas "arquivos" e "usuários", pois são usadas por outros módulos.



## Conectando versão local com BD em produção

1. Edite o arquivo ".env" que está na raiz do projeto. Comente as variáveis que definem a conexão com o banco local e descomente as variáveis que conectam ao banco em produção. Deixar o "APP_ENV" com o valor "production". Obs: Ao mudar o APP_ENV, alguns seed não executam, apenas os de categoria.

2. Salve o ".env", abra um terminal e execute `php artisan config:clear`. 

3. Seja cuidadoso.



## Observações

* Se a versão do PHP na máquina de desenvolvimento for diferente da versão na nuvem, podem ocorrer graves problemas.
A versão atual é 7.3 (falta atualizar em produção).

* Existe um comando para povoar apenas a tabela "modulo", mas está em desenvolvimento.

* O projeto "Carta de Serviços" necessita que o Legislarr esteja em execução na porta 8000.

* LEGISLARR e LOCATIONS: Se a modificação envolver criação de migrations, deve-se rodar as migrations em PRODUÇÃO antes de subir o código para produção.  
Essa ordem é importante, para evitar que o código quebre em produção. 

* Lembrar que ao adicionar ou atualizar uma dependência no projeto, deve também atualizar em produção (`git pull` + `composer update`).

* Se ocorrerem modificações em projetos relacionados que exijam mudanças na API do Legislarr, deve-se atualizar os dois projetos em produção (login ssh + `git pull`).
