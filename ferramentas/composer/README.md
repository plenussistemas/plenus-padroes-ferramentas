# Composer

## O que é?

Composer é uma ferramenta para gerenciamento de dependências em projetos PHP.
Com comandos simples, ele permite a inclusão, instalação e atualização dos pacotes de funcionalidade que um projeto utiliza.

Mais informações no [site oficial](https://getcomposer.org/).



## Como instalar?

1. Certifique-se de ter instalado e configurado o [**PHP**](../php/README.md) em sua máquina.

2. Acesse a [página de downloads](https://getcomposer.org/download/) do site oficial. 

3. Na seção "Windows Installer", clique no link para baixar o executável. Há também a opção de download por linha de comando, que não será tratada aqui.

4. Execute o instalador como administrador e siga as etapas.

5. Após a instalação, será necessário fechar qualquer janela de terminal (Cmd, PowerShell, Bash) para que o comando `composer` fique disponível.

6. TESTE: Crie uma pasta qualquer e acesse-a através da linha de comando. Execute o comando `composer init`, o qual deverá iniciar uma interação por texto para criar um projeto. 



## Como utilizar?

Os comandos do composer utilizam o arquivo "composer.json", que deve estar presente na raiz do projeto. Em projetos baseados em Laravel, esse arquivo já estará criado e configurado.


### Comandos básicos

`composer init` : Inicializa uma pasta, criando o arquivo "composer.json". 

`composer install` : Lê as dependências contidas no "composer.json" e inicia as instalações, armazenando os arquivos na pasta "vendor".

`composer update` : Busca as versões mais atuais das dependências definidas e substitui os pacotes que tiverem modificação.

`composer require {distribuidor/pacote}` : Adiciona o pacote como dependência no projeto e inicia sua instalação.

`composer remove {distribuidor/pacote}` : Remove o pacote da lista de dependências e inicia sua desinstalação.

`composer create-project {distribuidor/pacote}` : Cria um projeto baseado em um pacote de aplicação, já com suas dependências instaladas. 

