# Node.JS

## O que é?

NodeJS é um ambiente de execução para Javascript baseado na engine V8 (Google Chrome).
Com ele é possível executar códigos Javascript no lado do servidor.

Mais informações no [site oficial](https://nodejs.org/).



## Como instalar?

1. Acesse a [página de downloads](https://nodejs.org/en/download/) do site oficial. 

2. Selecione seu sistema operacional e arquitetura, lembrando de escolher a versão LTS. 

3. Execute o instalador como administrador e siga as etapas. 

4. Feche todas as janelas de terminal (se houver) para que ocorra atualização das variáveis.

5. Abra um terminal qualquer e digite `node -v` e `npm -v`. Devem aparecer as versões do NodeJS e NPM (gerenciador de pacotes).


## Como utilizar?

Siga os [tutoriais oficiais](https://nodejs.dev/learn) para aprender sobre o NodeJS.
