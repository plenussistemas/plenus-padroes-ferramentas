# WAMP Server

## O que é?

WAMPServer é uma aplicação que instala e controla versões pré-configuradas dos softwares Apache, MySQL e PHP, para que operem de forma integrada. Está disponível apenas no sistema operacional Windows.

Mais informações no [site oficial](https://www.wampserver.com/).



## Como instalar?

1. O bom comportamento do WAMPServer depende da presença de várias versões do "Microsoft Visual C++ Redistributable". Esses binários podem ser encontrados no [Centro de downloads da Microsoft](https://www.microsoft.com/en-in/download). Instale as versões de 2008 a 2015 (32 e 64 bits).

2. Acesse o [site oficial do WAMP](https://www.wampserver.com/) e navegue até a seção "Downloads". Baixe o executável adequado à sua arquitetura (X64 para 64 bits, X86 para 32 bits).

3. Execute o instalador como administrador e siga cuidadosamente.

4. Na tela de componentes, devem estar marcadas as opções "PHP 5.6.40", "PHP 7.3.x", "PHP 7.4.x" e "MySQL 5.7.x".

5. Conclua a instalação e reinicie a máquina se possível.



## Como utilizar?

No menu de aplicações haverá um ícone rosa com o título "WAMPServer64". Ao clicar nele, serão iniciados Apache, PHP e MySQL.

Um pequeno ícone colorido aparecerá na barra de tarefas. A cor desse ícone indica o estado da aplicação ou de seus componentes. 

Ao clicar no ícone, com o botão esquerdo, é possível:
* Criar e acessar virtualhosts

* Mudar a versão do PHP utilizada pelo Apache (não linha de comando)

* Modificar extensões e configurações do PHP ativo

* Reiniciar todos os componentes


Ao clicar no ícone, com o botão direito, surgem opções avançadas.

