# PHP

## O que é?

PHP é uma linguagem de programação muito utilizada no desenvolvimento web. 
Foi criada em 1995 por Rasmus Lerdorf e está atualmente na versão 7.4

Mais informações no [site oficial](https://www.php.net/).



## Como instalar?

### Windows

1. Siga o guia de instalação do [WAMP Server](../wamp-server/README.md).

2. Abra o "Painel de Controle" do Windows. Acesse "Sistema e Segurança" > "Sistema" > "Configurações avançadas do sistema". Na janela que abrir, clique no botão "Variáveis de Ambiente". 
Um atalho é abrir a pesquisa do Windows 10 e digitar "env".

3. Na primeira tabela de variáveis, selecione a linha que contém "Path" e clique em "editar".

4. Na tela de edição, clique em "Novo" e depois em "Procurar...".

5. Navegue até a pasta "C:/wamp64/bin/php/", onde existirão subpastas com números. 

6. Cada subpasta representa uma versão do PHP e não é possível associar mais de uma delas por vez, pois isso causaria conflito. Escolha uma versão recente, mas que também seja adequada a seu trabalho cotidiano.

7. Clique o botão "Ok" em todas as janelas que foram abertas nesse processo.

8. Feche todas as janelas de terminal (se houver) para que ocorra atualização das variáveis.

9. Abra um terminal qualquer e digite `php -v`. Deve aparecer um texto informativo com a versão do PHP.


### Linux

1. Verifique qual gerenciador de pacotes é utilizado por sua distribuição Linux, e como usá-lo.

2. Procure o pacote "php" e considere as opções disponíveis.

3. Instale o pacote como superusuário (sudo).

4. Geralmente, o comando é incluido na PATH automaticamente e não é necessário reiniciar.

5. Abra um terminal qualquer e digite `php -v`. Deve aparecer um texto informativo com a versão do PHP.


### MacOS

(TBD)



## Configurações adicionais

Um ambiente PHP é composto pela própria linguagem e por diversas extensões. O comportamento desse ambiente é modificado pelo arquivo "php.ini" presente pasta raiz da instalação. 
Em um arquivo INI, o caractere ";" comenta o conteúdo da linha.


Algumas extensões que devem ser habilitadas são:
* ftp (Legislarr)

* gd2 (Portyx Legado)

* mbstring (Laravel)

* mysqli (MySQL)

* pdo_mysql (MySQL)

* pdo_pgsql (PostgreSQL)
