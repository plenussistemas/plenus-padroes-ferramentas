# PostgreSQL

## O que é?

PostgreSQL é um sistema de gerenciamento de banco de dados relacional de código aberto.
É reconhecido por sua escalabilidade, desempenho e funcionalidades avançadas.

Mais informações no [site oficial](https://www.postgresql.org/).



## Como instalar?

1. Acesse a [página de downloads](https://www.postgresql.org/download/) do site oficial. 

2. Selecione seu sistema operacional. 

3. Para Windows, clique em "Download the installer" e escolha o binário adequado à sua arquitetura (32 ou 64 bits).

4. Execute o instalador como administrador e siga as etapas. Preste atenção ao caminho onde será instalado.

5. Abra o "Painel de Controle" do Windows. Acesse "Sistema e Segurança" > "Sistema" > "Configurações avançadas do sistema". Na janela que abrir, clique no botão "Variáveis de Ambiente". 
Um atalho é abrir a pesquisa do Windows 10 e digitar "env".

6. Na primeira tabela de variáveis, selecione a linha que contém "Path" e clique em "editar".

7. Na tela de edição, clique em "Novo" e depois em "Procurar...".

8. Navegue até a pasta onde o PostgreSQL foi instalado. Abra a subpasta "bin".

9. Clique o botão "Ok" em todas as janelas que foram abertas nesse processo.

10. Feche todas as janelas de terminal (se houver) para que ocorra atualização das variáveis.

11. Abra um terminal qualquer e digite `psql -V`. Deve aparecer um texto com a versão do PostgreSQL.



## Como utilizar?

Porta padrão é 5432.
Verificar se extensões foram habilitadas no PHP.
(tbd)