# Redis

## O que é?

Redis é um software para armazenamento de estruturas de dados em memória. Pode ser usado como banco de dados, message broker ou cache. 

Mais informações no [site oficial](https://redis.io/).



## Como instalar?

1. Na [página de downloads](https://redis.io/download) oficial está a versão mais atual, mas apenas para compilação ou para uso com o Docker. Por isso, utilizaremos uma versão alternativa.

2. Acesse o [repositório alternativo](https://github.com/microsoftarchive/redis/releases) mantido pela Microsoft e clique na versão mais recente (3.2.100).

3. Baixe os arquivos "Redis-x64-3.2.100", tanto o MSI quanto o ZIP.  

4. Execute o arquivo MSI e siga as etapas normalmente. Isso criará um serviço que será iniciado com o Windows.

5. Abra o "Gerenciador de Tarefas" do Windows, vá para a aba "Serviços" e procure pelo nome "Redis". O serviço deve existir e estar com status "rodando". 


## Como utilizar?

Leia a [documentação oficial](https://redis.io/documentation) caso tenha dúvidas.
