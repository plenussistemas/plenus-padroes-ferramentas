## Fork | Instalação

1. Acesse o [Site do Fork](https://git-fork.com/);
2. Faça o download do Fork clicando em "Download Fork for Windows";
3. Finalizado o download, é só instalar o Fork;

---

**OBS**: Não deixe de conferir o padrão [**Git**](../../padroes/git/README.md) usado na Empresa.
